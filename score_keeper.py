"""score_keeper.py

the class for keeping scores
"""

import json
from pathlib import Path

import click

import teksten


class ScoreKeeper:
    """keeps the scores, manages the scorefile """
    def __init__(self):
        """no args

        creates score dict
        can load and write to history file
        """
        self.score = dict(
            when=teksten.now(), win=0, loss=0, draw=0
        )

        file_name = 'history.json'
        script_dir = Path(__file__).parent
        self.history_file = Path(
            script_dir / file_name
        )
        if self.history_file.exists():
            self.load_history()
        else:
            self.history = list()

    def win(self):
        click.echo(teksten.win)
        self.score['win'] += 1

    def draw(self):
        click.echo(teksten.draw)
        self.score['draw'] += 1

    def lose(self):
        click.echo(teksten.lose)
        self.score['loss'] += 1

    def show_score(self):
        teksten.show_score(self.score)

    def dump_history(self):
        self.history.append(self.score)
        json.dump(
            self.history, self.history_file.open('w')
        )

    def load_history(self):
        self.history = json.load(
            self.history_file.open()
        )

    def show_history(self):
        teksten.show_history(
            self.history, self.calc_history()
        )

    def calc_history(self):
        total_win = 0
        total_loss = 0
        total_draw = 0
        for history in self.history:
            total_win += int(history['win'])
            total_loss += int(history['loss'])
            total_draw += int(history['draw'])

        return (
            f"wins: {total_win} | "
            f"losses: {total_loss} | "
            f"draws: {total_draw}"
        )
