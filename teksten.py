"""teksten.py

strings and operations
"""


from datetime import datetime
import click

algemene_help = """
[R] = rock
[P] = paper
[S] = scissor

Type one of these letters.

Paper wins against Rock
Rock wins against Scissors
Scissors wins against Paper

[q] quit
[x] score
[h] history
"""

prompt = "[?] > "
welcome = "let's play"
bye = "thanks for playing, boodgye"

win = "you win"
lose = "you lose"
draw = "you draw"
rock = "you played rock"
paper = "you played paper"
scissor = "you played scissor"
history_info = "(does not include current game)"


def now():
    """return isoformat of datetime now"""
    return datetime.isoformat(datetime.now())


def pretty_when(iso_string):
    """return nicer string of datetime isoformat"""
    from_iso = datetime.fromisoformat(iso_string)
    return (
        f"{int(from_iso.day):02}-"
        f"{int(from_iso.month):02}-"
        f"{from_iso.year} "
        f"{int(from_iso.hour):02}:"
        f"{int(from_iso.minute):02}"
    )


def show_score(score: dict, history=False):
    """click.echo passed in score

    score: score dict like from ScoreKeeper
    """
    if not history:
        click.echo("Score")
    click.echo(
        f"[{pretty_when(score['when'])}] | "
        f"wins: {score['win']} | "
        f"losses: {score['loss']} | "
        f"draws: {score['draw']}"
    )


def show_history(histories: list[dict], calc):
    """calls show_score for each history passed in"""
    click.echo("History")
    click.echo(f"{calc}")
    for history in histories:
        show_score(history, True)
