"""main.py

ssb steen schaar blad
Rock Paper Scissors

python3.10 main.py

requires python click
"""

import random

import click

import teksten
from score_keeper import ScoreKeeper

r = 2
p = 4
s = 8
rps = {
    2: "rock",
    4: "paper",
    8: "scissor",
}
sk = ScoreKeeper()


@click.command(
    context_settings=dict(
        help_option_names=('-h', '-?', '--help')
    )
)
@click.option(
    '-history',
    '--history',
    'history',
    is_flag=True,
    default=False,
    help="Show history and summary of scores",
)
def play(history):
    """main.play for ssb game

    will wait for correct input
    upon closing, writes a history file
    is managed by click
    """
    if history:
        return sk.show_history()
    click.echo(teksten.welcome)
    click.echo(teksten.algemene_help)
    while (
        ingang := click.prompt(teksten.prompt)
    ) != 'q':
        o = random.choice((r, s, p))
        match ingang:
            case '?':
                click.echo(teksten.algemene_help)
            case 'x':
                sk.show_score()
            case 'h':
                click.echo(teksten.history_info)
                sk.show_history()
            case 'R':
                p1 = r
                click.echo(
                    f"{teksten.rock} "
                    f"against {rps[o]}"
                )
                if p1 | o == r | s:
                    sk.win()
                elif p1 | o == r:
                    sk.draw()
                else:
                    sk.lose()
            case 'P':
                p1 = p
                click.echo(
                    f"{teksten.paper} "
                    f"against {rps[o]}"
                )
                if p1 | o == p | r:
                    sk.win()
                elif p1 | o == p:
                    sk.draw()
                else:
                    sk.lose()
            case 'S':
                p1 = s
                click.echo(
                    f"{teksten.scissor} "
                    f"against {rps[o]}"
                )
                if p1 | o == s | p:
                    sk.win()
                elif p1 | o == s:
                    sk.draw()
                else:
                    sk.lose()
    click.echo(teksten.bye)
    sk.dump_history()


if __name__ == '__main__':
    play()
